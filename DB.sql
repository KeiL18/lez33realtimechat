-- DB di progetto Chat
DROP DATABASE IF EXISTS Real_Time_Chat;
CREATE DATABASE Real_Time_Chat;
USE Real_Time_Chat;

CREATE TABLE Utente(
	id INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(100) NOT NULL,
    cognome VARCHAR(100) NOT NULL,
    ruolo VARCHAR(100) NOT NULL,
    nomeUtente VARCHAR(100) NOT NULL UNIQUE,
    pass VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Messaggio(
	id INTEGER NOT NULL AUTO_INCREMENT,
	nomeUtente VARCHAR(100),
    testo TEXT NOT NULL,
    dataOrario DATETIME,
    PRIMARY KEY (id)
    -- FOREIGN KEY (nomeUtente) REFERENCES Utente(nomeUtente) ON DELETE SET NULL
);

INSERT INTO Utente(nome,cognome,ruolo,nomeUtente,pass) VALUES
	("Irene","Quaglieri","UTENTE", "Irene", "Irene"),
    ("Andrea","Luca","UTENTE", "Andrea", "Andrea"),
    ("Giovanni","Pace","ADMIN", "Giovanni", "Giovanni");
    
INSERT INTO Messaggio(nomeUtente,testo,dataOrario) VALUES
	("Irene","Ciao", "2021-03-15 12:10:12"),
    ("Andrea","Ciao", "2021-03-15 12:11:30"),
    ("Irene","Come procede?", "2021-03-15 12:12:34"),
    ("Andrea","Tutto bene?", "2021-03-15 12:12:33"),
    ("Giovanni","Cancello tutti i messaggi", "2021-03-15 12:12:40");

-- CHAT CON REFERENCE KEY:
-- SELECT Utente.nomeUtente, Messaggio.testo, Messaggio.dataOrario FROM Messaggio 
-- 	JOIN Utente ON Utente.id = Messaggio.utenteid 
--    ORDER BY dataOrario;

-- CHAT:
SELECT nomeUtente,testo,dataOrario FROM Messaggio ORDER BY dataOrario; 

-- MESSAGGI DI UN UTENTE:
SELECT nomeUtente,testo,dataOrario FROM Messaggio 
	WHERE nomeUtente = "Andrea"
    ORDER BY dataOrario; 

-- DELETE FROM Utente WHERE id = 1;

-- UTENTI:
SELECT id,nome,cognome,ruolo,nomeUtente,pass FROM Utente;