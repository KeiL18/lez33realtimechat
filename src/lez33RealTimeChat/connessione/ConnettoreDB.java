package lez33RealTimeChat.connessione;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnettoreDB {

	private Connection conn;
	private static ConnettoreDB ogg_connessione;
	
	public static ConnettoreDB getIstanza() {
		if(ogg_connessione == null) {
			ogg_connessione = new ConnettoreDB();
			return ogg_connessione;
		}
		else {
			return ogg_connessione;
		}
	}
	
	public Connection getConnessione() throws SQLException{
		if(conn == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("127.0.0.1");  //equivale a localhost
			dataSource.setPortNumber(3306);
			dataSource.setUser("root");
			dataSource.setPassword("Toor");
			dataSource.setUseSSL(false);//settiamo in modo che possa comunicare anche in http senza ssl 
			dataSource.setDatabaseName("Real_Time_Chat");
			dataSource.setAllowPublicKeyRetrieval(true);	//Public key retrieval
			
			conn = dataSource.getConnection();
			return conn;
		}
		else {
			return conn;
		}
	}
	
}
