package lez33RealTimeChat.API;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import lez33RealTimeChat.models.Utente;
import lez33RealTimeChat.services.UtenteDAO;
import lez33RealTimeChat.utilities.ResponsoOperazione;

@WebServlet("/effettuaregistrazione")
public class RegistrazioneServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		ResponsoOperazione res = new ResponsoOperazione("Errore", "Metodo non permesso!");
		out.print(new Gson().toJson(res));
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		String varNome = request.getParameter("inputNomeR") != null ? request.getParameter("inputNomeR") : "";
		String varCognome = request.getParameter("inputCognomeR") != null ? request.getParameter("inputCognomeR") : "";
		String varUtente = request.getParameter("inputUserR") != null ? request.getParameter("inputUserR") : "";
		String varPass = request.getParameter("inputPasswordR") != null ? request.getParameter("inputPasswordR") : "";
		String varRuolo = request.getParameter("selectRuoloR") != null ? request.getParameter("selectRuoloR") : "UTENTE";
		
		if(varNome.isBlank() || varCognome.isBlank() || varUtente.isBlank() || varPass.isBlank()) {		
			ResponsoOperazione res = new ResponsoOperazione("Errore", "Inserisci tutti i valori nei campi del form di registrazione");
			out.print(new Gson().toJson(res));
			return;
		}
		
		try {
			if(userEsistente(varUtente)) {		
				ResponsoOperazione res = new ResponsoOperazione("Errore", "User gi� utilizzato");
				out.print(new Gson().toJson(res));
				return;
			}
		} catch (SQLException e1) {
			ResponsoOperazione res = new ResponsoOperazione("Errore", "Errore di interrogazione DB");
			out.print(new Gson().toJson(res));
			e1.printStackTrace();
			return;
		}
		
		Utente utente = new Utente();
		utente.setNome(varNome);
		utente.setCognome(varCognome);
		utente.setRuolo(varRuolo); //aggiungere la possibilit� di inserire il ruolo
		utente.setUser(varUtente);
		utente.setPassword(varPass);
		
		UtenteDAO uDao = new UtenteDAO();
		HttpSession sessione = request.getSession();
		
		try {
			uDao.insert(utente);
			sessione.setAttribute("isAuth", true);
			sessione.setAttribute("ruolo", utente.getRuolo());
			sessione.setAttribute("user", utente.getUser());
			if(request.getParameter("selectRuoloR") == null) {
				response.sendRedirect("http://localhost:8080/lez33RealTimeChat/chat.jsp");
			}
			else{
				response.sendRedirect("http://localhost:8080/lez33RealTimeChat/admin.html");
			}
		} catch (SQLException e) {
			ResponsoOperazione res = new ResponsoOperazione("Errore", "Non � stato possibile compliere la registrazione\n");
			out.print(new Gson().toJson(res));
			e.printStackTrace();
		}		
	}
	
	private boolean userEsistente(String varUser) throws SQLException {
		boolean esito = false;
		UtenteDAO uDao = new UtenteDAO();
		
		ArrayList<Utente> elenco = uDao.getAll();
		for(int i = 0; i<elenco.size(); i++) {
			Utente temp = elenco.get(i);
			if(temp.getUser().equals(varUser)) {
				esito = true;
			}
		}
		return esito;
	}
}
