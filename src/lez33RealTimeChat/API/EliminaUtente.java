package lez33RealTimeChat.API;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import lez33RealTimeChat.models.Utente;
import lez33RealTimeChat.services.UtenteDAO;

@WebServlet("/eliminautente")
public class EliminaUtente extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
				
		if(session.getAttribute("isAuth") == "true" && session.getAttribute("ruolo").equals("ADMIN")) {
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			UtenteDAO uDAO = new UtenteDAO();
			ArrayList<Utente> elencoUtenti = null;
			try {
				elencoUtenti = uDAO.getAll();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String risultatoJson = new Gson().toJson(elencoUtenti);
			out.print(risultatoJson);
		}
		else {
			response.sendRedirect("http://localhost:8080/lez33RealTimeChat/index.html");
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}

