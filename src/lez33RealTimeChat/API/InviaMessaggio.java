package lez33RealTimeChat.API;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import lez33RealTimeChat.models.Messaggio;
import lez33RealTimeChat.models.Utente;
import lez33RealTimeChat.services.MessaggioDAO;
import lez33RealTimeChat.services.UtenteDAO;
import lez33RealTimeChat.utilities.ResponsoOperazione;

@WebServlet("/inviamessaggio")
public class InviaMessaggio extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		ResponsoOperazione res = new ResponsoOperazione("Errore", "Metodo non permesso!");
		out.print(new Gson().toJson(res));
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		String varUtente = (String) session.getAttribute("user");
		String varMess = request.getParameter("messaggio") != null ? request.getParameter("messaggio") : "";
		
		if(varMess.isBlank()) {		
			response.sendRedirect("http://localhost:8080/lez33RealTimeChat/chat.jsp");
			return;
		}
		
		Messaggio mex = new Messaggio();
		mex.setUtente(varUtente);
		mex.setMessaggio(varMess);
		//System.out.println(LocalDateTime.now());
		mex.setDataOrario(LocalDateTime.now());
		
		HttpSession sessione = request.getSession();
		
		MessaggioDAO mDao = new MessaggioDAO();
		
		try {
			mDao.insert(mex);
			response.sendRedirect("http://localhost:8080/lez33RealTimeChat/chat.jsp");
			return;
		} catch (SQLException e) {
			ResponsoOperazione res = new ResponsoOperazione("Errore", "Errore recupero informazioni");
			out.print(new Gson().toJson(res));
			System.out.println(e.getMessage());
		}		
	}

}
