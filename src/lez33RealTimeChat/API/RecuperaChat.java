package lez33RealTimeChat.API;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import lez33RealTimeChat.models.Messaggio;
import lez33RealTimeChat.services.MessaggioDAO;
import lez33RealTimeChat.utilities.ResponsoOperazione;


@WebServlet("/scrivichat")
public class RecuperaChat extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		MessaggioDAO mDAO = new MessaggioDAO();
		
		try {
			
			ArrayList<Messaggio> elencoMessaggi = mDAO.getAll();
				
			String elencoJson = new Gson().toJson(elencoMessaggi);
			//System.out.println(elencoJson);
			
			ResponsoOperazione res = new ResponsoOperazione("OK", elencoJson);
			out.print(new Gson().toJson(res));
			
		} catch (SQLException e) {
			ResponsoOperazione res = new ResponsoOperazione("Errore", "Errore recupero informazioni");
			out.print(new Gson().toJson(res));
			System.out.println(e.getMessage());
		}

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
