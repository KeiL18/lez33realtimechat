package lez33RealTimeChat.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import lez33RealTimeChat.connessione.ConnettoreDB;
import lez33RealTimeChat.models.Utente;

public class UtenteDAO implements Dao<Utente>{

	@Override
	public Utente getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Utente> getAll() throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		ArrayList<Utente> elenco = new ArrayList<Utente>();
		
		String query = "SELECT id, nome, cognome, nomeUtente, ruolo, pass FROM Utente";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ps.executeQuery();
		ResultSet risultato = ps.getResultSet();
		
		while(risultato.next()) {
			Utente temp = new Utente();
			temp.setId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCognome(risultato.getString(3));
			temp.setUser(risultato.getString(4));
			temp.setRuolo(risultato.getString(5));
			temp.setPassword(risultato.getString(6));
			
			elenco.add(temp);
		}
		return elenco;
	}

	@Override
	public void insert(Utente t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "INSERT INTO Utente(nome, cognome, ruolo, nomeUtente, pass) VALUE (?, ?, ?, ?, ?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getCognome());
       	ps.setString(3, t.getRuolo());
       	ps.setString(4, t.getUser());
       	ps.setString(5, t.getPassword());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
	}

	@Override
	public boolean delete(int ID) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Utente t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean controllaUtente(Utente obj_utente) throws SQLException{
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT nomeUtente, pass, ruolo FROM Utente WHERE nomeUtente = ?";
		PreparedStatement ps =  conn.prepareStatement(query);
		ps.setString(1, obj_utente.getUser());
		
		ResultSet risultato = ps.executeQuery();
		
		boolean risposta = false;
		
		if(risultato.next()) {
			String passTemp = risultato.getString(2);
			if (passTemp.equals(obj_utente.getPassword())) {
				obj_utente.setRuolo(risultato.getString(3));
				return true;
			}
		}
		else {
			risposta = false;
		}
		return risposta;
	}
}
