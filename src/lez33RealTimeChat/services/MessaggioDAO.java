package lez33RealTimeChat.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import lez33RealTimeChat.connessione.ConnettoreDB;
import lez33RealTimeChat.models.Messaggio;

public class MessaggioDAO implements Dao<Messaggio>{

	@Override
	public Messaggio getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Messaggio> getAll() throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		ArrayList<Messaggio> elenco = new ArrayList<Messaggio>();
		
		String query = "SELECT id, nomeUtente, testo, dataOrario FROM Messaggio";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ps.executeQuery();
		ResultSet risultato = ps.getResultSet();
		
		while(risultato.next()) {
			Messaggio temp = new Messaggio();
			temp.setId(risultato.getInt(1));
			temp.setUtente(risultato.getString(2));
			temp.setMessaggio(risultato.getString(3));			
			
			String dataT = risultato.getString(4);
//			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
//			LocalDateTime dateTime = LocalDateTime.parse(dataT, formatter);
//			temp.setDataOrario(dateTime);
			temp.setDataOrario(dataT);
			
			elenco.add(temp);
		}
		return elenco;
	}

	@Override
	public void insert(Messaggio t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "INSERT INTO Messaggio( nomeUtente, testo, dataOrario) VALUE (?, ?, ?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getUtente());
		ps.setString(2, t.getMessaggio());
		ps.setString(3, t.getDataOrario().toString());
		
		ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
	}

	@Override
	public boolean delete(int ID) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Messaggio t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
