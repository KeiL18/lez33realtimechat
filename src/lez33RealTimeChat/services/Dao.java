package lez33RealTimeChat.services;

import java.sql.SQLException;
import java.util.ArrayList;

public interface Dao<T> {

	T getById(int id) throws SQLException;
	
	ArrayList<T> getAll() throws SQLException;
	
	void insert(T t) throws SQLException;
	
	boolean delete(int ID) throws SQLException;
	
	boolean update(T t) throws SQLException;

}
