package lez33RealTimeChat.models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Messaggio {
	
	private int id;
	private String messaggio;
	private String utente;
	private LocalDateTime dataOrario;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMessaggio() {
		return messaggio;
	}
	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;
	}
	public String getUtente() {
		return utente;
	}
	public void setUtente(String utente) {
		this.utente = utente;
	}
	public LocalDateTime getDataOrario() {
		return dataOrario;
	}
	
	public void setDataOrario(String dataOrario) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
		LocalDateTime dateTime = LocalDateTime.parse(dataOrario, formatter);
		this.dataOrario = dateTime;
	}
	
	public void setDataOrario(LocalDateTime dataOrario) {
		this.dataOrario = dataOrario;
	}
	
}
