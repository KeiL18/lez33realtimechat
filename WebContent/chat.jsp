<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    <%
    	HttpSession sessione = request.getSession();
    	String ruolo_utente = (String)sessione.getAttribute("isAuth") != null ? (String)sessione.getAttribute("isAuth") : "false";
    	
    	if(!ruolo_utente.equals("true"))
    		response.sendRedirect("index.html");
    %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
	  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<title>RTC - Chat</title>
	</head>
	<body>
		<!-- INIZIO: Navbar in alto -->
        <nav class="navbar navbar-expand-lg navbar-light bg-primary">
            <a class="navbar-brand" href="http://localhost:8080/lez33RealTimeChat/chat.jsp">RealTimeChat</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <ul class="navbar-nav mr-auto ">
		      <li class="nav-item">
		        <a class="nav-link" href="#">LogOut</a> <!--  TODO: demolire la sessione -->
		      </li>
		    </ul>
        </nav>
        <!-- FINE: Navbar in alto -->
        
        <div class="container">
	        <div class="row">
	        	<div class="col-md">
	        		<div class="form-group">
						<label for="chat">Chat:</label>
						<textarea class="form-control" id="chat" rows="20"></textarea>
					</div>
	        	</div>
	        </div>
	        <form name="registrazione" action="inviamessaggio" method="POST">
		        <div class="row">
		        	<div class="col-md 9">
		        		<textarea class="form-control" name="messaggio" rows="3"></textarea>
		        	</div>
		        	<div class="col-md-3 mt-2"><button type="submit" class="btn btn-primary btn-block">Invia</button></div>
		        </div>
	        </form>
        </div>
        
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<script src="js/script-chat.js"></script>
	</body>
</html>