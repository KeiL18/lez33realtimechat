$(document).ready(
    function(){
        aggiornaElenco();

        $("#refreshElenco").click(
			function(){
				aggiornaElenco();
			}
        );
    }
);

function aggiornaElenco(){
    $.ajax(
        {
            url: "http://localhost:8080/lez33RealTimeChat/user/list", //TODO: sviluppare la servlet per la richiesta
			method: "GET",
            success: function(ris_success){
					stampaElenco(ris_success);
            },
            error: function(ris_error){
					console.log("ERRORE");
					console.log(ris_error);
            }
        }
    );
}

function stampaElenco(arr_elenco){
	let elenco = "";
	
	for(let i=0; i<arr_elenco.length; i++){
		elenco += creaLineaElenco(arr_elenco[i]);
	}
	
	$("#elencoUtenti").html(elenco);
}

function creaLineaElenco(obj_utente){
	let linea = '<tr data-identificatore="' + obj_utente.user + '">';
	linea += '    <td>' + obj_utente.nome + '</td>';
	linea += '    <td>' + obj_utente.cognome + '</td>';
	linea += '    <td>' + obj_utente.user + '</td>';
	linea += '    <td>' + obj_utente.password + '</td>';
	linea += '    <td>' + obj_utente.ruolo + '</td>';
	linea += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaUtente(this)"><i class="fa fa-trash"></i></button></td>';
	linea += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaUtente(this)"><i class="fa fa-pencil"></i></button></td>';
	linea += '</tr>';
	
	return linea;
}