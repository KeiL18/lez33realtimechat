function stampaChat(chat){
	str = "";
	for(i=0; i<chat.length; i++){
		mexTemp = chat[i];
		strTemp = mexTemp.utente +", "+ mexTemp.dataOrario.date.day + "-"+ mexTemp.dataOrario.date.month + "-"+ mexTemp.dataOrario.date.year + ", ";
		strTemp += mexTemp.dataOrario.time.hour + ":"+ mexTemp.dataOrario.time.minute + ":\n";
		strTemp += "\t" + mexTemp.messaggio + "\n\n";
		str += strTemp;
	}
	$("#chat").val(str);
}

function aggiornaChat(){
	console.log("Sono in aggiorna pagina")
	$.ajax(
		{
			url: "http://localhost:8080/lez33RealTimeChat/scrivichat",
			method: "GET",
			success: function(risultato){
				console.log(risultato)
				switch(risultato.risultato){
					case "OK":
						let dettaglioJson = JSON.parse(risultato.dettaglio);
						stampaChat(dettaglioJson);
						break;
					case "ERRORE":
						alert("ERRORE:(\n" + risultato.dettaglio);
						break;
					}
			},
			error: function(risultato){
				console.log(risultato);
			},
		}		
		
	)
}

$(document).ready(
	function(){
		aggiornaChat();

		window.setInterval(
			function(){
				aggiornaChat();
				//console.log("Sto aggiornando la chat");			
			}
		, 1000);
	}

);